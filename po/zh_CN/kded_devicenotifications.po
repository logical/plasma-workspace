msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-19 01:58+0000\n"
"PO-Revision-Date: 2023-12-09 09:24\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-workspace/"
"kded_devicenotifications.pot\n"
"X-Crowdin-File-ID: 44499\n"

#: devicenotifications.cpp:275
#, kde-format
msgid "%1 has been plugged in."
msgstr "%1 已插入。"

#: devicenotifications.cpp:275
#, kde-format
msgid "A USB device has been plugged in."
msgstr "已插入 USB 设备。"

#: devicenotifications.cpp:278
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "检测到 USB 设备"

#: devicenotifications.cpp:295
#, kde-format
msgid "%1 has been unplugged."
msgstr "%1 已拔出。"

#: devicenotifications.cpp:295
#, kde-format
msgid "A USB device has been unplugged."
msgstr "已拔出 USB 设备。"

#: devicenotifications.cpp:298
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "已移除 USB 设备"
