# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-10-09 15:35+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: \n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: src/fingerprintmodel.cpp:151 src/fingerprintmodel.cpp:258
#, kde-format
msgid "No fingerprint device found."
msgstr "Не е намерено устройство за пръстов отпечатък."

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Retry scanning your finger."
msgstr "Опитайте повторно сканиране на пръста."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Плъзгането е прекалено кратко.Опитайте отново."

#: src/fingerprintmodel.cpp:335
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Пръстът не е центриран на четеца. Опитайте отново."

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Махнете пръста от четеца и опитайте отново."

#: src/fingerprintmodel.cpp:345
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Снемането на пръстовия отпечатък е неуспешно."

#: src/fingerprintmodel.cpp:348
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Няма достатъчно място на това устройство. Изтрийте пръстови отпечатъци, за "
"да продължите."

#: src/fingerprintmodel.cpp:351
#, kde-format
msgid "The device was disconnected."
msgstr "Връзката с устройството е прекъсната."

#: src/fingerprintmodel.cpp:356
#, kde-format
msgid "An unknown error has occurred."
msgstr "Възникна неизвестна грешка."

#: src/ui/ChangePassword.qml:27 src/ui/UserDetailsPage.qml:170
#, kde-format
msgid "Change Password"
msgstr "Промяна на парола"

#: src/ui/ChangePassword.qml:32
#, kde-format
msgid "Set Password"
msgstr "Задаване на парола"

#: src/ui/ChangePassword.qml:55
#, kde-format
msgid "Password"
msgstr "Парола"

#: src/ui/ChangePassword.qml:70
#, kde-format
msgid "Confirm password"
msgstr "Потвърждаване на парола"

#: src/ui/ChangePassword.qml:89 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "Паролите трябва да съвпадат"

#: src/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "Да се смени ли паролата на Портфейла?"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Сега, когато сте променили паролата си за влизане, може да искате да "
"промените паролата на вашия Kwallet, за да съвпада."

#: src/ui/ChangeWalletPassword.qml:30
#, kde-format
msgid "What is KWallet?"
msgstr "Какво е  KWallet?"

#: src/ui/ChangeWalletPassword.qml:40
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"Kwallet е мениджър на пароли, който съхранява паролите ви за безжични мрежи "
"и други криптирани ресурси. Той е заключен със собствена парола, която се "
"различава от паролата за влизане. Ако двете пароли съвпадат, тя може да бъде "
"отключена автоматично при влизане, така че не е нужно да въвеждате паролата "
"на Kwallet."

#: src/ui/ChangeWalletPassword.qml:56
#, kde-format
msgid "Change Wallet Password"
msgstr "Смяна на парола на Портфейла"

#: src/ui/ChangeWalletPassword.qml:65
#, kde-format
msgid "Leave Unchanged"
msgstr "Оставяне непроменено"

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "Създаване на потребител"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:132
#, kde-format
msgid "Name:"
msgstr "Име:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:140
#, kde-format
msgid "Username:"
msgstr "Потребителско име:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Standard"
msgstr "Стандартно"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Administrator"
msgstr "Администратор"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:154
#, kde-format
msgid "Account type:"
msgstr "Вид на акаунта:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "Парола:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "Потвърждаване на парола:"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "Създаване"

#: src/ui/FingerprintDialog.qml:45
#, kde-format
msgid "Configure Fingerprints"
msgstr "Конфигуриране на отпечатъци"

#: src/ui/FingerprintDialog.qml:55
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "Изчистване на всичко"

#: src/ui/FingerprintDialog.qml:62
#, kde-format
msgid "Add"
msgstr "Добавяне"

#: src/ui/FingerprintDialog.qml:71
#, kde-format
msgid "Cancel"
msgstr "Отказване"

#: src/ui/FingerprintDialog.qml:79
#, kde-format
msgid "Done"
msgstr "Завършен"

#: src/ui/FingerprintDialog.qml:103
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Снемане на отпечатък"

#: src/ui/FingerprintDialog.qml:112
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно десния си показалец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:114
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно десния си среден пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:116
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно десния си безименен пръст върху сензора за "
"пръстови отпечатъци."

#: src/ui/FingerprintDialog.qml:118
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно десния си малък пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:120
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно десния си палец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:122
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно левия си показалец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:124
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно левия си среден пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно левия си безименен пръст върху сензора за "
"пръстови отпечатъци."

#: src/ui/FingerprintDialog.qml:128
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно левия си малък пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:130
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr ""
"Моля, натиснете многократно левия си палец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:134
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно десния си показалец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:136
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно десния си среден пръст върху сензора за "
"пръстови отпечатъци."

#: src/ui/FingerprintDialog.qml:138
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно десния си безименен пръст върху сензора за "
"пръстови отпечатъци."

#: src/ui/FingerprintDialog.qml:140
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно десния си малък пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:142
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно десния си палец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:144
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно левия си показалец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:146
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно левия си среден пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:148
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно левия си безименен пръст върху сензора за "
"пръстови отпечатъци."

#: src/ui/FingerprintDialog.qml:150
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно левия си малък пръст върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:152
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr ""
"Моля, прокарайте многократно левия си палец върху сензора за пръстови "
"отпечатъци."

#: src/ui/FingerprintDialog.qml:167
#, kde-format
msgid "Finger Enrolled"
msgstr "Пръстът е регистриран"

#: src/ui/FingerprintDialog.qml:197
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Сложете пръст за вземане на отпечатък"

#: src/ui/FingerprintDialog.qml:315
#, kde-format
msgid "Re-enroll finger"
msgstr "Сложете отново пръста"

#: src/ui/FingerprintDialog.qml:322
#, kde-format
msgid "Delete fingerprint"
msgstr "Изтриване на отпечатъци"

#: src/ui/FingerprintDialog.qml:331
#, kde-format
msgid "No fingerprints added"
msgstr "Не са добавени отпечатъци"

#: src/ui/main.qml:20
#, kde-format
msgid "Users"
msgstr "Потребители"

#: src/ui/main.qml:31
#, kde-format
msgctxt "@action:button As in, 'add new user'"
msgid "Add New"
msgstr "Добавяне на нов"

#: src/ui/main.qml:107
#, kde-format
msgctxt "@info:usagetip"
msgid "Press Space to edit the user profile"
msgstr "Натиснете интервал, за да редактирате потребителския профил"

#: src/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "Промяна на аватара"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "Това не е нищо"

#: src/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr "Feisty Flamingo"

#: src/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "Dragon's Fruit"

#: src/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "Sweet Potato"

#: src/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr "Ambient Amber"

#: src/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr "Sparkle Sunbeam"

#: src/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr "Lemon-Lime"

#: src/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr "Verdant Charm"

#: src/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr "Mellow Meadow"

#: src/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr "Tepid Teal"

#: src/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "Plasma Blue"

#: src/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr "Pon Purple"

#: src/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr "Bajo Purple"

#: src/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "Burnt Charcoal"

#: src/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr "Paper Perfection"

#: src/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr "Cafétera Brown"

#: src/ui/PicturesSheet.qml:38
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "Rich Hardwood"

#: src/ui/PicturesSheet.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "Връщане назад"

#: src/ui/PicturesSheet.qml:78
#, kde-format
msgctxt "@action:button"
msgid "Initials"
msgstr "Инициали"

#: src/ui/PicturesSheet.qml:129
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "Избиране на файл…"

#: src/ui/PicturesSheet.qml:134
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "Избиране на снимка"

#: src/ui/PicturesSheet.qml:181
#, kde-format
msgctxt "@action:button"
msgid "Placeholder Icon"
msgstr "Заместваща икона"

#: src/ui/PicturesSheet.qml:269
#, kde-format
msgctxt "@info:whatsthis"
msgid "User avatar placeholder icon"
msgstr "Използване на заместваща икона аватар"

#: src/ui/UserDetailsPage.qml:111
#, kde-format
msgid "Change avatar"
msgstr "Промяна на аватара"

#: src/ui/UserDetailsPage.qml:164
#, kde-format
msgid "Email address:"
msgstr "Адрес за е-поща:"

#: src/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Delete files"
msgstr "Изтриване на файлове"

#: src/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Keep files"
msgstr "Запазване на файлове"

#: src/ui/UserDetailsPage.qml:207
#, kde-format
msgid "Delete User…"
msgstr "Изтриване на потребител…"

#: src/ui/UserDetailsPage.qml:219
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Конфигуриране на идентификация с пръстов отпечатък…"

#: src/ui/UserDetailsPage.qml:248
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Пръстовите отпечатъци могат да се използват вместо парола, когато отключвате "
"екрана и при предоставяне на администраторски разрешения за приложения и "
"команди, които ги изискват.<nl/><nl/> Влизане в системата с пръстов "
"отпечатък все още не се поддържа."

#: src/user.cpp:280
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Не можа да се получи разрешение за запазване на потребител %1"

#: src/user.cpp:285
#, kde-format
msgid "There was an error while saving changes"
msgstr "При запазването на промените възникна грешка"

#: src/user.cpp:384
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Неуспех при преоразмеряване на изображение: отварянето на временния файл "
"пропадна"

#: src/user.cpp:392
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Неуспех при преоразмеряване на изображение: записът във временния файл "
"пропадна"

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "Вашия акаунт"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "Други акаунти"

#~ msgid "Manage Users"
#~ msgstr "Управление на потребителите"
