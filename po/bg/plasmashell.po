# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2022, 2023 Mincho Kondarev <mkondarev@yahoo.de>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-09 01:37+0000\n"
"PO-Revision-Date: 2023-11-11 16:24+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: \n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Минчо Кондарев"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "mkondarev@yahoo.de"

#: currentcontainmentactionsmodel.cpp:206
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Конфигуриране на приставката за действия на мишката"

#: desktopview.cpp:211
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Dev"
msgstr "KDE Plasma 6.0 Dev"

#: desktopview.cpp:214
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Alpha"
msgstr "KDE Plasma 6.0 Alpha"

#: desktopview.cpp:217
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 1"
msgstr "KDE Plasma 6.0 Beta 1"

#: desktopview.cpp:220
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma 6.0 Beta 2"
msgstr "KDE Plasma 6.0 Beta 2"

#: desktopview.cpp:223
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC1"
msgstr "KDE Plasma 6.0 RC1"

#: desktopview.cpp:226
#, kde-format
msgctxt "@label %1 is the Plasma version, RC meaning Release Candidate"
msgid "KDE Plasma 6.0 RC2"
msgstr "KDE Plasma 6.0 RC2"

#: desktopview.cpp:252
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "KDE Plasma %1 Dev"

#: desktopview.cpp:256
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "KDE Plasma %1 Beta"

#: desktopview.cpp:259
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr "KDE Plasma %1 Beta %2"

#: desktopview.cpp:263
#, kde-format
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "KDE Plasma %1"

#: desktopview.cpp:269
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr "Посетете bugs.kde.org, за да докладвате за грешки"

#: main.cpp:88
#, kde-format
msgid "Plasma Shell"
msgstr "Обвивка на Plasma"

#: main.cpp:100
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Активиране на QML Javascript откриване на грешки"

#: main.cpp:103
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Без автоматично рестартиране на Plasma обвивка след срив"

#: main.cpp:106
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Принудително зареждане на дадената приставка на обвивката"

#: main.cpp:110
#, kde-format
msgid "Replace an existing instance"
msgstr "Заменяне на съществуваща инстанция"

#: main.cpp:113
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Активира тестовия режим и определя файла с оформление на javascript, за да "
"настрои среда за тестване"

#: main.cpp:114
#, kde-format
msgid "file"
msgstr "файл"

#: main.cpp:118
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Списък на наличните опции за обратна връзка от потребителите"

#: main.cpp:200
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma не успя да стартира"

#: main.cpp:201
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma не може да се стартира, тъй като не може да използва правилно OpenGL "
"2 или резервен софтуер\n"
"Моля, проверете дали вашите графични драйвери са настроени правилно."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Аудиото е изключено"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Микрофонът е изключен"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 е заглушен"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Тъчпадът е включен"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Тъчпадът е изключен"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi включен"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi изключен"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth е включен"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth е изключен"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Мобилен интернет е включен"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Мобилен интернет е изключен"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "Екранна клавиатура е активирана"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "Екранна клавиатура е деактивирана"

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr "Вътрешен екран на %1"

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr "%1 %2 on %3"

#: shellcontainmentconfig.cpp:132
#, kde-format
msgid "Disconnected Screen %1"
msgstr "Прекъснат екран %1"

#: shellcorona.cpp:198 shellcorona.cpp:200
#, kde-format
msgid "Show Desktop"
msgstr "Показване на работния плот"

#: shellcorona.cpp:200
#, kde-format
msgid "Hide Desktop"
msgstr "Скриване на работния плот"

#: shellcorona.cpp:216
#, kde-format
msgid "Show Activity Switcher"
msgstr "Показване на превключвател на дейности"

#: shellcorona.cpp:227
#, kde-format
msgid "Stop Current Activity"
msgstr "Спиране на текущата дейност"

#: shellcorona.cpp:235
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Превключване към предишна дейност"

#: shellcorona.cpp:243
#, kde-format
msgid "Switch to Next Activity"
msgstr "Превключване към следваща дейност"

#: shellcorona.cpp:258
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Активиране на запис от управление на задачите %1"

#: shellcorona.cpp:285
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr "Настройване на работния плот и панелите..."

#: shellcorona.cpp:307
#, kde-format
msgid "Move keyboard focus between panels"
msgstr "Преместване на фокуса на клавиатурата между панелите"

#: shellcorona.cpp:2047
#, kde-format
msgid "Add Panel"
msgstr "Добавяне на панел"

#: shellcorona.cpp:2083
#, kde-format
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "Празно %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Използван софтуерен рендерер"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Използван софтуерен рендерер"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "Рендирането може да бъде влошено"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "Никога повече не показвай"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Брой панели"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Изброява панелите"

#~ msgctxt "@label %1 version string"
#~ msgid "Plasma %1 Development Build"
#~ msgstr "Plasma %1 Development Build"
