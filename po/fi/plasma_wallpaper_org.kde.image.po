# Finnish messages for plasma_wallpaper_image.
# Copyright © 2010 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdebase package.
# Teemu Rytilahti <teemu.rytilahti@d5k.net>, 2008.
# Tommi Nieminen <translator@legisign.org>, 2010, 2014, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Lasse Liehu <lasse.liehu@gmail.com>, 2010, 2011, 2012, 2013, 2014, 2015.
# Jorma Karvonen <karvonen.jorma@gmail.com>, 2010.
#
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-09-26 18:19+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:22:37+0000\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tommi Nieminen"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "translator@legisign.org"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, kde-format
msgctxt "@title:window"
msgid "Open Image"
msgstr "Avaa kuva"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, kde-format
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Kansio, josta kuvia näytetään"

#: imagepackage/contents/ui/config.qml:123
#, kde-format
msgid "Positioning:"
msgstr "Sijoittelu:"

#: imagepackage/contents/ui/config.qml:126
#, kde-format
msgid "Scaled and Cropped"
msgstr "Skaalattu ja rajattu"

#: imagepackage/contents/ui/config.qml:130
#, kde-format
msgid "Scaled"
msgstr "Skaalattu"

#: imagepackage/contents/ui/config.qml:134
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Skaalattu, säilytä suhteet"

#: imagepackage/contents/ui/config.qml:138
#, kde-format
msgid "Centered"
msgstr "Keskitetty"

#: imagepackage/contents/ui/config.qml:142
#, kde-format
msgid "Tiled"
msgstr "Monistettu"

#: imagepackage/contents/ui/config.qml:170
#, kde-format
msgid "Background:"
msgstr "Tausta:"

#: imagepackage/contents/ui/config.qml:171
#, kde-format
msgid "Blur"
msgstr "Sumenna"

#: imagepackage/contents/ui/config.qml:180
#, kde-format
msgid "Solid color"
msgstr "Tasainen väri"

#: imagepackage/contents/ui/config.qml:191
#, kde-format
msgid "Select Background Color"
msgstr "Valitse taustan väri"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Avaa taustakuvatiedosto"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Seuraava taustakuva"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, kde-format
msgid "Images"
msgstr "Kuvat"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr "Lisää…"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, kde-format
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Hae uusia…"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Avaa yläkansio"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Palauta taustakuva"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Poista taustakuva"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr "Tällä työkalulla voi asettaa kuvan Plasma-istunnon taustakuvaksi."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Kuvatiedosto tai asennettu taustakuvapaketti, jonka haluat Plasma-istunnon "
"taustakuvaksi"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"Taustakuvatiedoston nimessä on yksittäinen puolilainausmerkki ('): pyydä "
"taustakuvan tekijää korjaamaan nimi tai korjaa se itse: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "Plasman taustakuvaa asetettaessa sattui virhe:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr "Kaikki työpöytien taustakuvaksi asennettiin onnistuneesti paketti %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr ""
"Kaikki työpöytien taustakuvaksi asennettiin onnistuneesti kuvatiedosto %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"Taustakuvaksi asetettavaa tiedostoa ei löydy tai sitä ei voida tunnistaa "
"taustakuvaksi: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Kuvatiedostot"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:45
#, kde-format
msgid "Order:"
msgstr "Järjestys:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:52
#, kde-format
msgid "Random"
msgstr "Satunnainen"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:56
#, kde-format
msgid "A to Z"
msgstr "Nimen mukaan"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:60
#, kde-format
msgid "Z to A"
msgstr "Nimen mukaan käänteisesti"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:64
#, kde-format
msgid "Date modified (newest first)"
msgstr "Muokkauspäivän mukaan uusin ensin"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:68
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Muokkauspäivän mukaan vanhin ensin"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:93
#, kde-format
msgid "Group by folders"
msgstr "Ryhmittele kansioittain"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:105
#, kde-format
msgid "Change every:"
msgstr "Kuvanvaihtoväli:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:115
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 tunti"
msgstr[1] "%1 tuntia"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:135
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuutti"
msgstr[1] "%1 minuuttia"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:155
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekunti"
msgstr[1] "%1 sekuntia"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:186
#, kde-format
msgid "Folders"
msgstr "Kansiot"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:190
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr "Lisää…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:228
#, kde-format
msgid "Remove Folder"
msgstr "Poista kansio"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:239
#, kde-format
msgid "Open Folder…"
msgstr "Avaa kansio…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:254
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "Taustakuvien sijainteja ei ole asetettu"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:49
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Aseta taustakuvaksi"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:52
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Työpöytä"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:58
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Lukitusnäyttö"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:64
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Kumpikin"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:97
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr ""
"Plasman taustakuvaa asetettaessa tapahtui virhe:\n"
"%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:110
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr "Yritettäessä avata kscreenlockerrc-asetustiedostoa tapahtui virhe."

#~ msgid "Add Image…"
#~ msgstr "Lisää kuva…"

#~ msgid "Add Folder…"
#~ msgstr "Lisää kansio…"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Suositeltu taustakuvatiedosto"

#~ msgid "There are no wallpapers in this slideshow"
#~ msgstr "Tässä diaesityksessä ei ole taustakuvia"

#~ msgid "Add Custom Wallpaper"
#~ msgstr "Lisää oma taustakuva"

#~ msgid "Remove wallpaper"
#~ msgstr "Poista taustakuva"

#~ msgid "%1 by %2"
#~ msgstr "%1, tekijä %2"

#, fuzzy
#~| msgid "Remove Wallpaper"
#~ msgid "Wallpapers"
#~ msgstr "Poista taustakuva"

#~ msgctxt "<image> by <author>"
#~ msgid "By %1"
#~ msgstr "Tekijä: %1"

#~ msgid "Download Wallpapers"
#~ msgstr "Lataa taustakuvia"

#~ msgid "Hours"
#~ msgstr "tuntia"

#~ msgid "Open..."
#~ msgstr "Avaa…"

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Tuntematon"

#~ msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
#~ msgstr "Kuvatiedostot (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"

#~ msgid "Screenshot"
#~ msgstr "Kuvankaappaus"

#~ msgid "Preview"
#~ msgstr "Esikatselu"
