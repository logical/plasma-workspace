# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2014, 2016, 2017.
# Tommi Nieminen <translator@legisign.org>, 2016, 2017, 2018, 2020, 2022, 2023.
#
# Larso, 2014.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-12 01:38+0000\n"
"PO-Revision-Date: 2023-09-26 18:16+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/AlbumArtStackView.qml:202
#, kde-format
msgid "No title"
msgstr "Ei otsikkoa"

#: package/contents/ui/AlbumArtStackView.qml:202
#: package/contents/ui/main.qml:54
#, kde-format
msgid "No media playing"
msgstr "Mikään ei soi"

#: package/contents/ui/ExpandedRepresentation.qml:383
#: package/contents/ui/ExpandedRepresentation.qml:504
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "−%1"

#: package/contents/ui/ExpandedRepresentation.qml:532
#, kde-format
msgctxt "@action:button"
msgid "Shuffle"
msgstr "Sekoita"

#: package/contents/ui/ExpandedRepresentation.qml:556
#: package/contents/ui/main.qml:111
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Edellinen kappale"

#: package/contents/ui/ExpandedRepresentation.qml:578
#: package/contents/ui/main.qml:119
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Tauko"

#: package/contents/ui/ExpandedRepresentation.qml:578
#: package/contents/ui/main.qml:127
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Toista"

#: package/contents/ui/ExpandedRepresentation.qml:596
#: package/contents/ui/main.qml:135
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Seuraava kappale"

#: package/contents/ui/ExpandedRepresentation.qml:619
#, kde-format
msgid "Repeat Track"
msgstr "Toista kappale"

#: package/contents/ui/ExpandedRepresentation.qml:619
#, kde-format
msgid "Repeat"
msgstr "Toista"

#: package/contents/ui/main.qml:57
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"artistilta %1 (%2)\n"
"Keskinapsautuksella tauko\n"
"Säädä äänenvoimakkuuden vierittämällä"

#: package/contents/ui/main.qml:58
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"%1\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""
"%1\n"
"Tauko keskinapsautuksella\n"
"Säädä äänenvoimakkuutta vierittämällä"

#: package/contents/ui/main.qml:60
#, kde-format
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"artistilta %1 (tauko, %2)\n"
"Tauko keskinapsautuksella\n"
"Säädä äänenvoimakkuutta vierittämällä"

#: package/contents/ui/main.qml:61
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"Paused (%1)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr ""
"Tauko (%1)\n"
"Toista keskinapsautuksella\n"
"Säädä äänenvoimakkuutta vierittämällä"

#: package/contents/ui/main.qml:104
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Avaa"

#: package/contents/ui/main.qml:143
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Pysäytä"

#: package/contents/ui/main.qml:156
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Lopeta"

#~ msgid "Choose player automatically"
#~ msgstr "Valitse soitin automaattisesti"

#~ msgid "General"
#~ msgstr "Yleistä"

#~ msgid "Volume step:"
#~ msgstr "Äänenvoimakkuuden askel:"

#~ msgctxt "by Artist (player name)"
#~ msgid "by %1 (%2)"
#~ msgstr "artistilta %1 (%2)"

#~ msgctxt "Paused (player name)"
#~ msgid "Paused (%1)"
#~ msgstr "Tauko (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "artistilta %1"

#~ msgid "Pause playback when screen is locked"
#~ msgstr "Keskeytä toisto näytön ollessa lukittu"
