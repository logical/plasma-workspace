# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Volkan Gezer <volkangezer@gmail.com>, 2022.
# Emir SARI <emir_sari@icloud.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-10 01:59+0000\n"
"PO-Revision-Date: 2023-08-10 22:24+0300\n"
"Last-Translator: Emir SARI <emir_sari@icloud.com>\n"
"Language-Team: Turkish <kde-l10n-tr@kde.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.11.70\n"

#: kcm.cpp:73
#, kde-format
msgid "Toggle do not disturb"
msgstr "Rahatsız Etmeyin kipini aç/kapat"

#: sourcesmodel.cpp:393
#, kde-format
msgid "Other Applications"
msgstr "Diğer Uygulamalar"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "Açılır pencere göster"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Rahatsız Etmeyin kipinde göster"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Geçmişte göster"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "Bildirim rozetlerini göster"

#: ui/ApplicationConfiguration.qml:164
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Olayları Yapılandır"

#: ui/ApplicationConfiguration.qml:172
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr ""
"Bu uygulama, bildirimlerin olay bazında yapılandırılmasını desteklemiyor."

#: ui/ApplicationConfiguration.qml:261
#, kde-format
msgid "Show a message in a pop-up"
msgstr "İletiyi bir açılır pencerede göster"

#: ui/ApplicationConfiguration.qml:270
#, kde-format
msgid "Play a sound"
msgstr "Bir ses çal"

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Bildirimleri görüntülemek için gerekli olan bir 'Bildirim' araç takımı "
"bulunamıyor. Sistem Tepsinizde veya tek başına bir uygulama olarak etkin "
"olduğundan emin olun."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "Bildirimler şu anda Plasma yerine '%1 %2' tarafından sağlanıyor."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Bildirimler şu anda Plasma tarafından sağlanmıyor."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Rahatsız Etmeyin kipi"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "Etkinleştir:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Ekranlar yansıtıldığında"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Ekran paylaşımı sırasında"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "Klavye kısayolu:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Görünürlük koşulları"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Kritik bildirimler:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Rahatsız Etmeyin kipinde göster"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Olağan bildirimler:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Tam ekran pencerelerin üzerinde göster"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Düşük öncelikli bildirimler:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Açılır pencere göster"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Açılır pencereler"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Konum:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Bildirim simgesi yakınında"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Özel Konum Seç…"

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 saniye"
msgstr[1] "%1 saniye"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Şundan sonra gizle:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Ek geri bildirim"

#: ui/main.qml:250
#, kde-format
msgid "Application progress:"
msgstr "Uygulama ilerlemesi:"

#: ui/main.qml:251 ui/main.qml:291
#, kde-format
msgid "Show in task manager"
msgstr "Görev yöneticisinde göster"

#: ui/main.qml:263
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Bildirimlerde göster"

#: ui/main.qml:277
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "İlerleme sırasında açılır pencereyi açık tut"

#: ui/main.qml:290
#, kde-format
msgid "Notification badges:"
msgstr "Bildirim rozetleri:"

#: ui/main.qml:302
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Uygulamaya özel ayarlar"

#: ui/main.qml:307
#, kde-format
msgid "Configure…"
msgstr "Yapılandır…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Açılır Pencere Konumu"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "Uygulama Ayarları"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "Uygulamalar"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "Sistem Hizmetleri"

#: ui/SourcesPage.qml:148
#, kde-format
msgid "No application or event matches your search term"
msgstr "Arama teriminizle hiçbir uygulama veya olay eşleşmiyor."

#: ui/SourcesPage.qml:171
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""
"Bildirim ayarlarını ve davranışlarını yapılandırmak için listeden bir "
"uygulama seçin."
